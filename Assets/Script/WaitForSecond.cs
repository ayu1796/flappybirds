﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaitForSecond : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private UnityEvent OnComplete;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Wait(float seconds)
    {
        StartCoroutine(IeWaitForSecond(seconds));
    }
    private IEnumerator IeWaitForSecond(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (OnComplete != null)
        {
            OnComplete.Invoke();
        }
    }
}
