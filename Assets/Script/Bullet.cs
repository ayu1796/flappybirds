﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    // Public variable 
    public  int speed = 6;
    private Rigidbody2D r2d;

    void Start()
    {
        // Get the rigidbody component
        r2d = GetComponent<Rigidbody2D>();

        // Make the bullet right
        r2d.velocity = new Vector3(speed, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    // Function called when the object goes out of the screen
    void OnBecameInvisible()
    {
        // Destroy the bullet 
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        Bullet bullet = collision.gameObject.GetComponent<Bullet>();

        if (bullet)
        {

            Collider2D collider = GetComponent<Collider2D>();

            string name = collider.gameObject.name;
            Debug.Log("nama " + name);
            // If the enemy collided with a bullet
            if (name == "pipeUp(Clone)" || name == "pipeDown(Clone)")
            {
                // Destroy itself (the enemy) and the bullet
                Destroy(gameObject);
                Destroy(collider.gameObject);
            }
        }
    }

}
